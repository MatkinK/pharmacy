#!venv/bin/python
from json_rpc_server import server
from config import SERVER_PORT

print("Server started at port %d" % SERVER_PORT)
server.serve_forever()
