SERVER_PORT = 8000

OPENDATA_URL = 'http://opendata.permkrai.ru/opendata/5902293308-medicaments/data-20160115-structure-20160115.csv' # Url to drug list csv
SQLALCHEMY_ROOT_URL = 'mysql://root:1@localhost/'
SQLALCHEMY_DATABASE = 'pharmacy'
SQLALCHEMY_TABLES = ['drugs', 'baskets']
