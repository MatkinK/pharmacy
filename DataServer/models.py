from config import SQLALCHEMY_TABLES
from sqlalchemy import Table, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Drug(Base):
    """ Препарат """
    __tablename__ = 'drugs'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    form = Column(String(1000))

    def __init__(self, id, name, form):
        self.id = id
        self.name = name
        self.form = form

    def get_table(metadata):
        return Table(SQLALCHEMY_TABLES[0], metadata,
                     Column('id', Integer, primary_key=True),
                     Column('name', String(255)),
                     Column('form', String(255)))
