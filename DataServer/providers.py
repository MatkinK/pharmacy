import requests
from models import Drug, Base
from config import OPENDATA_URL, SQLALCHEMY_ROOT_URL, SQLALCHEMY_DATABASE
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
import simplejson as json


class DataProvider:

    def download_file(self, url, local_filename):
        """ Downloading remote file """
        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
        return local_filename

    def parse_csv(filepath):
        """ Parsing drugs csv file """
        with open(filepath, 'r', newline=None, encoding='utf-8') as f:
            f.readline()  # Skip header
            id = 0
            for line in f.readlines():
                id += 1
                columns = line.split(';')
                name = columns[0]
                form = columns[4]
                if form[-1] == '\n':
                    form = form[:-1]
                yield Drug(id, name, form)

    def get_drugs(self):
        """ Get drug list """
        session = self.Session()
        drugs = [{'id': d.id, 'name': d.name.encode('utf-8'), 'form': d.form}
                 for d in session.query(Drug).all()]
        print('Request for drugs')
        res = json.dumps(drugs, ensure_ascii=False)
        return res

    def create_database(self):
        self.engine = create_engine(SQLALCHEMY_ROOT_URL+SQLALCHEMY_DATABASE,
                                    echo=True, pool_recycle=7200)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)

    def add_drugs_to_database(self, drugs):
        session = self.Session()
        session.add_all(drugs)
        session.commit()

    def connect(self):
        """ Connect to database """
        self.prepare()
        self.create_database()
        session = self.Session()
        if session.query(Drug).count() == 0:
            print('Drug table is empty. Add downloaded drug list')
            drugs_from_file = [d for d in DataProvider.parse_csv('data.csv')]
            self.add_drugs_to_database(drugs_from_file)

    def disconnect(self):
        """ Disconnect from database """
        pass

    def prepare(self):
        """ Check database or create it otherwise """
        if os.path.isfile("data.csv"):
            print("Data file already exists")
        else:
            print('Start download csv file')
            self.download_file(OPENDATA_URL, "data.csv")
            print("Data file downloaded")
