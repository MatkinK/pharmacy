#!venv/bin/python
# encoding: utf8

from xmlrpc.server import SimpleXMLRPCServer
from config import SERVER_PORT
from providers import DataProvider


def get_drugs():
    return dataprovider.get_drugs()

dataprovider = DataProvider()
dataprovider.connect()

server = SimpleXMLRPCServer(('0.0.0.0', SERVER_PORT))
server.register_function(get_drugs, 'get_drugs')
